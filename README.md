
This program is an example of how to read sampled sound files, specifically wav, mp3, or aac files.

It will read the file and then convert it to a wav file. It can also dump the samples as csv file.

## Build

```bash
mvn clean package
```

## Run

### File

If the desired input file is on the local computer, we'll run it like so:

```bash
java -jar target/sound-sampled.jar <filename>
```

### URL

If the desired input file is a URL, we'll run the program like so:

```bash
java -jar target/sound-sampled.jar --url <url>
```

## Set Output File

The wav file is saved as `output.wav` by default, but we can change it with the `--output` flag:

```bash
java -jar target/sound-sampled.jar $HOME/input.mp3 --output example.wav
```

## Dump CSV File

We can dump the samples of the wav file as a csv file with the `--csv` flag.

```bash
java -jar target/sound-sampled.jar $HOME/input.mp3 --csv
```

By default the csv file is saved as `output.csv`, but pass in the file name with the `--csv` flag for a different filename.

```bash
java -jar target/sound-sampled.jar $HOME/input.mp3 --csv example.csv
```

