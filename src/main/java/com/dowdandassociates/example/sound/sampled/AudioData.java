/*
 *
 * sound-sampled-example/src/main/java/com/dowdandassociates/example/sound/sampled/AudioData.java
 *
 *------------------------------------------------------------------------------
 * Copyright 2015-2018 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

package com.dowdandassociates.example.sound.sampled;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Convert audio to a common format and read it into memory.
 *
 */
public class AudioData
{
    private static final Logger log = LoggerFactory.getLogger(AudioData.class);

    // Buffer length for reading audio stream for conversions.
    private static final int BUFFER_LENGTH = 8192;

    // Common audio format
    //
    // 16 and 24 bit sample sizes are common for distribution and 32 bit is
    // common for mixing and mastering, we'll use 32 bit sample size to
    // keep from losing data and because it aligns with one of Java's
    // primitive number types.
    // *  8 bit = byte
    // * 16 bit = short
    // * 32 bit = int
    // * 64 bit = long
    //
    // If a different size were used, remember to change frame size of the
    // audio format as well. Also splitChannels would have to be changed to
    // use the new data type.
    // Frame size for common audio format
    private static final byte COMMON_SAMPLE_SIZE = 4;

    private byte[] bytes;
    private AudioFormat audioFormat;
    ByteBuffer byteBuffer;

    public AudioData(AudioInputStream audioInputStream) throws IOException
    {
        AudioFormat sourceFormat = audioInputStream.getFormat();

        audioFormat = new AudioFormat(
                AudioFormat.Encoding.PCM_SIGNED,                    // Using signed PCM because all Java number types are signed.
                sourceFormat.getSampleRate(),                       // Keep the source sample rate
                COMMON_SAMPLE_SIZE * 8,                             // 32 bit sample size (4 bytes)
                sourceFormat.getChannels(),                         // Keep the same number of channels as the source
                sourceFormat.getChannels() * COMMON_SAMPLE_SIZE,    // Frame size is 4 bytes (32 bits) * the number of channels
                sourceFormat.getSampleRate(),                       // Have the frame rate be the same as the sample rate
                (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN)); // Use native byte ordering

        bytes = convertAudio(audioInputStream, audioFormat);

        byteBuffer = ByteBuffer.wrap(bytes);
        // ByteBuffer is BIG_ENDIAN by default
        if (!audioFormat.isBigEndian())
        {
            byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        }
    }

    /**
     *
     * Get the bytes of the audio after being converted into a common audio
     * format.
     *
     * @return Bytes of the audio data
     *
     */
    public byte[] getBytes()
    {
        return bytes;
    }

    /**
     *
     * Get the audio format the input was converted to.
     *
     * @return Audio format of the audio data.
     */
    public AudioFormat getAudioFormat()
    {
        return audioFormat;
    }

    /**
     *
     * Get the converted data as an AudioInputStream.
     *
     * @return AudioInputStream of the audio data.
     */
    public AudioInputStream getAudioInputStream()
    {
        return createAudioInputStream(bytes, audioFormat);
    }

    /**
     *
     * Get the bytes of the audio format as a ByteBuffer.
     *
     * @return ByteBuffer of the audio data
     *
     */
    public ByteBuffer getByteBuffer()
    {
        return byteBuffer;
    }

    /**
     *
     * Split the audio data into separate channels.
     *
     * Combines the bytes of the frame into an int so byte order does not
     * matter.
     *
     * The first index of the array is the channel. The second index is the
     * position of the sample.
     *
     * @return The audio data split into channels and with the data frames
     * combined into ints.
     *
     */
    public int[][] splitChannels()
    {
        IntBuffer intBuffer = byteBuffer.asIntBuffer();
        int channels = audioFormat.getChannels();
        int limit = intBuffer.limit();
        int length = limit / channels;
        int[][] data = new int[channels][length];

        for (int index = 0; index < length; ++index)
        {
            for (int channel = 0; channel < channels; ++channel)
            {
                data[channel][index] = intBuffer.get();
            }
        }

        return data;
    }

    /**
     * Convert audio to the given audio format
     *
     * @param sourceAIS The source audio input stream.
     * @param audioFormat The target audio format.
     *
     * @return Bytes of the converted audio.
     */
    public static byte[] convertAudio(AudioInputStream sourceAIS, AudioFormat audioFormat) throws IOException
    {
        if (null == sourceAIS)
        {
            throw new IllegalArgumentException("Source audio input stream not set.");
        }

        if (null == audioFormat)
        {
            throw new IllegalArgumentException("Target audio format not set.");
        }

        // The source audio format.
        AudioFormat sourceFormat = sourceAIS.getFormat();

        // MP3s will have unknown sample size, which will be set as a negative
        // number. Testing so far shows to use 16 bit sample size.
        int bitDepth = (sourceFormat.getSampleSizeInBits() < 0) ? 16 : 32;

        AudioFormat intermediateFormat = new AudioFormat(
                AudioFormat.Encoding.PCM_SIGNED,                    // Use signed PCM because all Java number types are signed.
                sourceFormat.getSampleRate(),                       // Keep the same sample rate as the input.
                bitDepth,                                           // Use the sample size we just determined above.
                sourceFormat.getChannels(),                         // Keep the same number of channels.
                sourceFormat.getChannels() * bitDepth / 8,          // Frame size is the number of channels times the number of bytes per frame.
                sourceFormat.getSampleRate(),                       // Have the frame rate be the same as the sample rate.
                (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN)); // Use the native byte ordering.
        try
            (
            // Convert the source to the intermediate format.
            AudioInputStream intermediateAIS = AudioSystem.getAudioInputStream(intermediateFormat, sourceAIS);
            // Convert the intermediate format the the target format.
            AudioInputStream convertAIS = AudioSystem.getAudioInputStream(audioFormat, intermediateAIS);
            // OutputStream to collect bytes of converted audio
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            )
        {
            byte[] buffer = new byte[BUFFER_LENGTH];
            while (true)
            {
                int bytesRead = convertAIS.read(buffer, 0, buffer.length);
                if (-1 == bytesRead)
                {
                    break;
                }
                baos.write(buffer, 0, bytesRead);
            }
            return baos.toByteArray();
        }
    }

    /**
     *
     * Convert audio to the given audio format.
     *
     * @param fromBytes audio data to convert from
     * @param fromFormat audio format to convert from
     * @param toFormat audio format to convert to
     *
     * @return Bytes of the converted audio.
     */
    public static byte[] convertAudio(byte[] fromBytes, AudioFormat fromFormat, AudioFormat toFormat) throws IOException
    {
        try
            (
            AudioInputStream fromAis = createAudioInputStream(fromBytes, fromFormat);
            )
        {
            return convertAudio(fromAis, toFormat);
        }
    }

    /**
     *
     * Take byte array and AudioFormat and turn it into an AudioInputStream
     *
     * @param bytes byte array of audio data
     * @param audioFormat audio format of audio data
     *
     * @return AudioInputStream of given data
     */
    public static AudioInputStream createAudioInputStream(byte[] bytes, AudioFormat audioFormat)
    {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        AudioInputStream ais = new AudioInputStream(
                bais,
                audioFormat,
                bytes.length / audioFormat.getFrameSize());
        return ais;
    }
}

