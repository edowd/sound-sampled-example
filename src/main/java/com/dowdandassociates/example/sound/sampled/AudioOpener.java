/*
 *
 * sound-sampled-example/src/main/java/com/dowdandassociates/example/sound/sampled/AudioOpener.java
 *
 *------------------------------------------------------------------------------
 * Copyright 2015-2016 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

package com.dowdandassociates.example.sound.sampled;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import net.sourceforge.jaad.spi.javasound.AACAudioFileReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Open the audio input stream and get the audio file format.
 */
public class AudioOpener
{
    private static final Logger log = LoggerFactory.getLogger(AudioOpener.class);

    private AudioInputStream audioInputStream;
    private AudioFileFormat audioFileFormat;

    public AudioOpener(File file) throws IOException, UnsupportedAudioFileException
    {
        // While JAAD has support for working with AudioSystem, it does not
        // give back the correct audio format. This workaround gets to the
        // correct behavior, but requires this special handling.
        if (isAAC(file.getPath()))
        {
            audioInputStream = new AACAudioFileReader().getAudioInputStream(file);
            AudioFormat audioFormat = audioInputStream.getFormat();
            audioFileFormat = new AudioFileFormat(AACAudioFileReader.AAC, audioFormat, AudioSystem.NOT_SPECIFIED);
        }
        else
        {
            audioFileFormat = AudioSystem.getAudioFileFormat(file);
            audioInputStream = AudioSystem.getAudioInputStream(file);
        }
    }

    public AudioOpener(URL url) throws IOException, UnsupportedAudioFileException
    {
        // Same workaround as File version, just for opening the audio stream
        // with a URL instead.
        if (isAAC(url.getPath()))
        {
            audioInputStream = new AACAudioFileReader().getAudioInputStream(url);
            AudioFormat audioFormat = audioInputStream.getFormat();
            audioFileFormat = new AudioFileFormat(AACAudioFileReader.AAC, audioFormat, AudioSystem.NOT_SPECIFIED);
        }
        else
        {
            audioFileFormat = AudioSystem.getAudioFileFormat(url);
            audioInputStream = AudioSystem.getAudioInputStream(url);
        }
    }

    /**
     * Check if the path is an AAC file based off the file extension.
     *
     * @param path The path to the audio file
     *
     * @return Whether we think the audio file is an AAC file.
     */
    private boolean isAAC(String path)
    {
        String parsedPath = path.toLowerCase().trim();

        return (parsedPath.endsWith(".aac") ||
                parsedPath.endsWith(".m4a") ||
                parsedPath.endsWith(".m4b") ||
                parsedPath.endsWith(".m4p") ||
                parsedPath.endsWith(".m4r") ||
                parsedPath.endsWith(".3gp") ||
                parsedPath.endsWith(".mp4"));
    }

    public AudioInputStream getAudioInputStream()
    {
        return audioInputStream;
    }

    public AudioFileFormat getAudioFileFormat()
    {
        return audioFileFormat;
    }
}

