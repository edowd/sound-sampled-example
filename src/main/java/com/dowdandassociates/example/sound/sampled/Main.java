/*
 *
 * sound-sampled-example/src/main/java/com/dowdandassociates/example/sound/sampled/Main.java
 *
 *------------------------------------------------------------------------------
 * Copyright 2015-2016 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

package com.dowdandassociates.example.sound.sampled;

import java.io.File;
import java.net.URL;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class Main
{
    private static final String HELP_CMD_LINE_SYNTAX = "sound-sampled [OPTIONS] [FILE]";
    private static final String DEFAULT_WAV_PATH = "output.wav";
    private static final String DEFAULT_CSV_PATH = "output.csv";

    public static void main(String[] args)
    {
        Option helpOption = Option.builder("?").
                longOpt("help").
                desc("Print this help message").
                build();

        Option urlOption = Option.builder("u").
                longOpt("url").
                desc("File path is a URL").
                build();

        Option outputOption = Option.builder("o").
                longOpt("output").
                hasArg().
                optionalArg(true).
                argName("FILE").
                desc("Output wav file (default: " + DEFAULT_WAV_PATH + ")").
                build();

        Option csvOption = Option.builder("c").
                longOpt("csv").
                hasArg().
                optionalArg(true).
                argName("FILE").
                desc("Output csv file (default: " + DEFAULT_CSV_PATH + ")").
                build();

        Option playOption = Option.builder("p").
                longOpt("play").
                desc("Play file").
                build();

        Options options = new Options();
        options.addOption(helpOption);
        options.addOption(urlOption);
        options.addOption(outputOption);
        options.addOption(csvOption);
        options.addOption(playOption);

        HelpFormatter formatter = new HelpFormatter();

        CommandLineParser parser = new DefaultParser();

        try
        {
            CommandLine cmdLine = parser.parse(options, args);

            if (cmdLine.hasOption("?"))
            {
                formatter.printHelp(HELP_CMD_LINE_SYNTAX, options);
                System.exit(0);
            }

            List<String> remainingArgs = cmdLine.getArgList();
            if (remainingArgs.isEmpty())
            {
                throw new MissingArgumentException("FILE not set");
            }
            String path = remainingArgs.get(0);

            AudioOpener audioOpener;
            if (cmdLine.hasOption("u"))
            {
                System.out.println("Open URL: " + path);
                audioOpener = new AudioOpener(new URL(path));
            }
            else
            {
                System.out.println("Open File: " + path);
                audioOpener = new AudioOpener(new File(path));
            }

            AudioPismo.printInputInfo(audioOpener);

            AudioData audioData = new AudioData(audioOpener.getAudioInputStream());

            AudioPismo.printConvertedInfo(audioData);

            if (cmdLine.hasOption("o"))
            {
                String wavPath = cmdLine.getOptionValue("o");
                if (null == wavPath)
                {
                    wavPath = DEFAULT_WAV_PATH;
                }
                AudioPismo.writeWav(wavPath, audioData);
            }

            if (cmdLine.hasOption("c"))
            {
                String csvPath = cmdLine.getOptionValue("c");
                if (null == csvPath)
                {
                    csvPath = DEFAULT_CSV_PATH;
                }
                AudioPismo.writeCsv(csvPath, audioData);
            }

            if (cmdLine.hasOption("p"))
            {
                AudioPismo.playAudio(audioData.getBytes(), audioData.getAudioFormat());
            }
        }
        catch (MissingOptionException | MissingArgumentException mxe)
        {
            System.out.println(mxe.getMessage());
            formatter.printHelp(HELP_CMD_LINE_SYNTAX, options);
            System.exit(1);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(1);
        }
    }
}

