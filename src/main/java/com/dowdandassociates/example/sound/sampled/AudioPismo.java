/*
 *
 * sound-sampled-example/src/main/java/com/dowdandassociates/example/sound/sampled/AudioPismo.java
 *
 *------------------------------------------------------------------------------
 * Copyright 2015-2018 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

package com.dowdandassociates.example.sound.sampled;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.List;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import net.sourceforge.jaad.spi.javasound.AACAudioFileReader;

public class AudioPismo
{
    public static void printInputInfo(AudioOpener audioOpener)
    {
        AudioFileFormat audioFileFormat = audioOpener.getAudioFileFormat();
        AudioInputStream audioInputStream = audioOpener.getAudioInputStream();
        AudioFormat audioFormat = audioInputStream.getFormat();

        System.out.println();
        System.out.println("Input File Format");
        System.out.println("-----------------");
        System.out.println("Byte Length: " + audioFileFormat.getByteLength());
        System.out.println("Frame Length: " + audioFileFormat.getFrameLength());
        System.out.println("Type: " + audioFileFormat.getType());
        System.out.println("Properties: " + audioFileFormat.properties());
        System.out.println(audioFileFormat.toString());

        System.out.println();
        System.out.println("Input Format");
        System.out.println("------------");
        System.out.println("Channels: " + audioFormat.getChannels());
        System.out.println("Encoding: " + audioFormat.getEncoding());
        System.out.println("Frame Rate: " + audioFormat.getFrameRate());
        System.out.println("Frame Size: " + audioFormat.getFrameSize());
        System.out.println("Sample Rate: " + audioFormat.getSampleRate());
        System.out.println("Sample Size In Bits: " + audioFormat.getSampleSizeInBits());
        System.out.println("Is Big Endian? " + audioFormat.isBigEndian());
        System.out.println(audioFormat.toString());
    }

    public static void printConvertedInfo(AudioData audioData)
    {
        byte[] audioBytes = audioData.getBytes();
        AudioFormat targetFormat = audioData.getAudioFormat();
        ByteBuffer audioByteBuffer = audioData.getByteBuffer();
        IntBuffer audioBuffer = audioByteBuffer.asIntBuffer();

        System.out.println();
        System.out.println("Converted Data");
        System.out.println("--------------");
        System.out.println("audioBytes.length = " + audioBytes.length);
        System.out.println("audioByteBuffer.isDirect() = " + audioByteBuffer.isDirect());
        System.out.println("audioByteBuffer.hasArray() = " + audioByteBuffer.hasArray());
        System.out.println("audioByteBuffer.limit() = " + audioByteBuffer.limit());
        System.out.println("audioBuffer.isDirect() = " + audioBuffer.isDirect());
        System.out.println("audioBuffer.hasArray() = " + audioBuffer.hasArray());
        System.out.println("audioBuffer.limit() = " + audioBuffer.limit());

        System.out.println();
        System.out.println("Converted Format");
        System.out.println("----------------");
        System.out.println("Channels: " + targetFormat.getChannels());
        System.out.println("Encoding: " + targetFormat.getEncoding());
        System.out.println("Frame Rate: " + targetFormat.getFrameRate());
        System.out.println("Frame Size: " + targetFormat.getFrameSize());
        System.out.println("Sample Rate: " + targetFormat.getSampleRate());
        System.out.println("Sample Size In Bits: " + targetFormat.getSampleSizeInBits());
        System.out.println("Is Big Endian? " + targetFormat.isBigEndian());

        System.out.println(targetFormat.toString());
    }

    public static void writeWav(String wavPath, AudioData audioData) throws IOException
    {
        try
            (
            AudioInputStream outputAIS = audioData.getAudioInputStream();
            )
        {
            System.out.println();
            System.out.println("Save to: " + wavPath);

            File outputFile = new File(wavPath);
            int bytesWritten = AudioSystem.write(outputAIS, AudioFileFormat.Type.WAVE, outputFile);
        }
    }

    public static void writeCsv(String csvPath, AudioData audioData) throws IOException
    {
        int[][] split = audioData.splitChannels();
        System.out.println();
        System.out.println("Output CSV: " + csvPath);
        try
            (
            FileOutputStream fos = new FileOutputStream(csvPath);
            OutputStreamWriter osw = new OutputStreamWriter(fos, "utf-8");
            Writer writer = new BufferedWriter(osw);
            )
        {
            if (split.length > 0 && split[0].length > 0)
            {
                StringBuilder strbuf = new StringBuilder().append("sample");
                for (int channel = 0, len = split.length; channel < len; ++channel)
                {
                    strbuf.append(",channel").append(channel);
                }
                strbuf.append('\n');
                writer.write(strbuf.toString());
                for (int i = 0, len = split[0].length; i < len; ++i)
                {
                    strbuf.setLength(0);
                    strbuf.append(i);
                    for (int channel = 0, channels = split.length; channel < channels; ++channel)
                    {
                        strbuf.append(',').append(split[channel][i]);
                    }
                    strbuf.append('\n');
                    writer.write(strbuf.toString());
                }
            }
        }
    }

    public static void playAudio(byte[] audioBytes, AudioFormat audioFormat)
            throws InterruptedException, IOException, LineUnavailableException
    {
        try
            (
            AudioInputStream outputAis = AudioData.createAudioInputStream(audioBytes, audioFormat);
            )
        {
            playAudio(outputAis);
        }
    }

    public static void playAudio(AudioInputStream audioInputStream)
            throws InterruptedException, IOException, LineUnavailableException
    {
        AudioFormat audioFormat = audioInputStream.getFormat();

        int bitDepth = 16;
        float sampleRate = audioFormat.getSampleRate();
        int channels = audioFormat.getChannels();
        boolean bigEndian = false;

        AudioFormat outputFormat = new AudioFormat(
                AudioFormat.Encoding.PCM_SIGNED,
                sampleRate,
                bitDepth,
                channels,
                channels * bitDepth / 8,
                sampleRate,
                bigEndian);

        byte[] outputBytes = AudioData.convertAudio(audioInputStream, outputFormat);

        try
            (
            AudioInputStream outputAis = AudioData.createAudioInputStream(outputBytes, outputFormat);
            )
        {
            playAudioDirect(outputAis);
        }
    }

    public static void playAudioDirect(AudioInputStream audioInputStream)
            throws InterruptedException,
                   IOException,
                   LineUnavailableException
    {
        AudioListener listener = new AudioListener();

        AudioFormat audioFormat = audioInputStream.getFormat();
        System.out.println();
        System.out.println("Playing Format");
        System.out.println("----------------");
        System.out.println("Channels: " + audioFormat.getChannels());
        System.out.println("Encoding: " + audioFormat.getEncoding());
        System.out.println("Frame Rate: " + audioFormat.getFrameRate());
        System.out.println("Frame Size: " + audioFormat.getFrameSize());
        System.out.println("Sample Rate: " + audioFormat.getSampleRate());
        System.out.println("Sample Size In Bits: " + audioFormat.getSampleSizeInBits());
        System.out.println("Is Big Endian? " + audioFormat.isBigEndian());
        System.out.println(audioFormat.toString());

        Clip clip = AudioSystem.getClip();
        clip.addLineListener(listener);
        clip.open(audioInputStream);
        try
        {
            clip.start();
            listener.waitUntilDone();
        }
        finally
        {
            clip.close();
        }
    }

    static class AudioListener implements LineListener
    {
        private boolean done = false;

        @Override
        public synchronized void update(LineEvent event)
        {
            LineEvent.Type eventType = event.getType();
            if (LineEvent.Type.OPEN == eventType)
            {
                System.out.println("AudioListener: Open");
            }
            else if (LineEvent.Type.START == eventType)
            {
                System.out.println("AudioListener: Start");
            }
            else if (LineEvent.Type.STOP == eventType)
            {
                System.out.println("AudioListener: Stop");
                setDone();
            }
            else if (LineEvent.Type.CLOSE == eventType)
            {
                System.out.println("AudioListener: Close");
                setDone();
            }
            else
            {
                System.out.println("AudioListener: Unknown line event type: " + eventType);
            }
        }

        private synchronized void setDone()
        {
            done = true;
            notifyAll();
        }

        public synchronized void waitUntilDone()
                throws InterruptedException
        {
            while (!done)
            {
                wait();
            }
        }
    }
}

